<?php
/**
* BSS Commerce Co.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://bsscommerce.com/Bss-Commerce-License.txt
*
* =================================================================
*                 MAGENTO EDITION USAGE NOTICE
* =================================================================
* This package designed for Magento COMMUNITY edition
* BSS Commerce does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* BSS Commerce does not provide extension support in case of
* incorrect edition usage.
* =================================================================
*
* @category   BSS
* @package    Bss_MultiStoreViewPricing
* @author     Extension Team
* @copyright  Copyright (c) 2015-2016 BSS Commerce Co. ( http://bsscommerce.com )
* @license    http://bsscommerce.com/Bss-Commerce-License.txt
*/
class Bss_MultiStoreViewPricing_Model_Observer {
	public function getFinalPrice($observer) {
		if(Mage::helper('multistoreviewpricing')->isScopePrice() == 0) return;
		
		$product = $observer->getProduct();
		$old_final_price = $product->getFinalPrice();
		$productIds = array($product->getId());
		$prices = $this->productListPriceWorkaround($productIds);
		$special_prices = $this->productListSpecialPriceWorkaround($productIds);
		if(isset($prices[$product->getId()])) {
			$product->setPrice($prices[$product->getId()]);
			$product->setMinimalPrice($prices[$product->getId()]);
			$info = Mage::getVersionInfo();
			if($info['major'] == 1 && $info['minor'] <= 5) {
				$price = Mage::getModel('multistoreviewpricing/catalogrule')->calcProductPriceRule15($product,$product->getPrice());
			}else {
				$price = Mage::getModel('multistoreviewpricing/catalogrule')->calcProductPriceRule($product,$product->getPrice());
			}
			$product->setFinalPrice($price);
			// if($product->getSpecialPrice() && $product->getSpecialPrice() < $price) {
			// 	$product->setFinalPrice($product->getSpecialPrice());
			// }
		}

		if(isset($special_prices[$product->getId()])) {
			$specialPriceFromDate = strtotime($product->getSpecialFromDate());
			$specialPriceToDate = strtotime($product->getSpecialToDate());
			$today =  time();
			if( ($specialPriceFromDate == '' && $specialPriceToDate == '') || ($specialPriceFromDate == '' && $today <= $specialPriceToDate ) || ($specialPriceToDate == '' && $today >= $specialPriceFromDate) || ($today >= $specialPriceFromDate && $today <= $specialPriceToDate) ) {
				if(!$price) {
					$price = Mage::getModel('multistoreviewpricing/catalogrule')->calcProductPriceRule($product,$product->getPrice());
				}
				if($price > $special_prices[$product->getId()]) {
					$product->setFinalPrice($special_prices[$product->getId()]);
				}else {
					$product->setFinalPrice($price);
				}
			}
		}

		// if($product->getFinalPrice() > $old_final_price) {
		// 	$product->setFinalPrice($old_final_price);
		// }

		return $this;
	}

	public function categoryListPrice($observer) {
		if(Mage::helper('multistoreviewpricing')->isScopePrice() == 0) return;

		$collection = $observer->getCollection();
		foreach($collection as $product){
			switch ($product->getTypeId()) {
				case 'bundle':
				$prices = Mage::helper('multistoreviewpricing')->getPriceBundleCategory($product);
				$product->setMinPrice($prices['min']);
				$product->setMaxPrice($prices['max']);
				break;
				
				case 'grouped':
				$price = Mage::helper('multistoreviewpricing')->getPriceGroupedCategory($product);
				$product->setMinimalPrice($price);
				break;
			}
			//Mage::dispatchEvent('catalog_product_get_final_price', array('product' => $product, 'qty' => 1));
		}
	}

	public function rendererAttributes($observer) {
		$form = $observer->getEvent()->getForm();
		$tierPrice = $form->getElement('tier_price_for_store');
		$groupPrice = $form->getElement('group_price_for_store');
		if ($tierPrice) {
			if(Mage::registry('current_product') && Mage::registry('current_product')->getTypeId() == 'bundle') {
				$tierPrice->setRenderer(
					Mage::app()->getLayout()->createBlock('multistoreviewpricing/catalog_product_edit_tab_price_tier')
					->setPriceColumnHeader(Mage::helper('bundle')->__('Percent Discount'))
					);
			}else {
				$tierPrice->setRenderer(
					Mage::app()->getLayout()->createBlock('multistoreviewpricing/catalog_product_edit_tab_price_tier')
					);
			}
		}

		if ($groupPrice) {
			if(Mage::registry('current_product') && Mage::registry('current_product')->getTypeId() == 'bundle') {
				$groupPrice->setRenderer(
					Mage::app()->getLayout()->createBlock('multistoreviewpricing/catalog_product_edit_tab_price_group')
					->setPriceColumnHeader(Mage::helper('bundle')->__('Percent Discount'))
					);
			}else {
				$groupPrice->setRenderer(
					Mage::app()->getLayout()->createBlock('multistoreviewpricing/catalog_product_edit_tab_price_group')
					);
			}
		}
	}


	public function saveProductAfter($observer) {
		$product = $observer->getEvent()->getProduct();
		$store = Mage::app()->getRequest()->getParam('store',0);
		if ($store > 0 && $product && $product->getId() > 0) {

			$default_value_tier = Mage::app()->getRequest()->getParam('tier_price_store_view_default',0);
			$model = Mage::getModel('multistoreviewpricing/tierDefault')->getCollection()
			->addFieldToSelect('*')
			->addFieldToFilter('product_id', $product->getId())
			->addFieldToFilter('store_id', $store)
			->getFirstItem();

			$model->setProductId($product->getId())
			->setStoreId($store)
			->setStatus($default_value_tier)
			->save();
		}
	}


	private function productListPriceWorkaround(array $productIds){
		foreach($productIds as $index => $prodId){
			if(!is_numeric($prodId))unset($productIds[$index]);
		}
		if(count($productIds) < 1)return array();
		$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
        //We will trust the $productIds array as we check above that it only contains numbers
		$query = $dbRead->query("SELECT entity_id, value FROM ". Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_decimal') . "
			WHERE attribute_id = (SELECT attribute_id FROM " . Mage::getSingleton('core/resource')->getTableName('eav/attribute') . " WHERE attribute_code = 'price' AND backend_model != '' LIMIT 1)
			AND entity_id IN (" . implode(",", $productIds) . ")
			AND store_id IN(0,". Mage::app()->getStore()->getStoreId() .")
			ORDER BY store_id ASC");
		$results = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){ //Specify FETCH_ASSOC to save memory as we know the column names are all we need
        	$results[$row["entity_id"]] = $row["value"];
        }
        return $results;
    }

    private function productListSpecialPriceWorkaround(array $productIds){
    	foreach($productIds as $index => $prodId){
    		if(!is_numeric($prodId))unset($productIds[$index]);
    	}
    	if(count($productIds) < 1)return array();
    	$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
        //We will trust the $productIds array as we check above that it only contains numbers
    	$query = $dbRead->query("SELECT entity_id, value FROM ". Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_decimal') . "
    		WHERE attribute_id = (SELECT attribute_id FROM " . Mage::getSingleton('core/resource')->getTableName('eav/attribute') . " WHERE attribute_code = 'special_price' AND backend_model != '' LIMIT 1)
    		AND entity_id IN (" . implode(",", $productIds) . ")
    		AND store_id IN(0,". Mage::app()->getStore()->getStoreId() .")
			ORDER BY store_id ASC");
    	$results = array();
        while($row = $query->fetch(PDO::FETCH_ASSOC)){ //Specify FETCH_ASSOC to save memory as we know the column names are all we need
        	$results[$row["entity_id"]] = $row["value"];
        }
        return $results;
    }
}
